﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vindinium
{
    class ConflictResolver
    {
        public enum ActOnPos
        {
            Stay,
            ForceGo,
            RiskyToGo
        }

        public class ConflictResolveResult
        {
            public ActOnPos Result { get; set; }
            public Pos Pos { get; set; }
        }
        public EnemyInfo IsConflict(CurrentState state)
        {
            var nearest = state.Enemies.OrderBy(x => x.Path == null ? 100 : x.Path.Count).First();
            if (nearest.Path?.Count < 5)
                return nearest;
            return null;
        }

        public ConflictResolveResult ActOnConflict(EnemyInfo enemy, CurrentState state)
        {
            var retVal = new ConflictResolveResult
            {
                Result = ActOnPos.Stay
            };
            var pathStr = string.Join(",", enemy.Path);
            var minesDiff = (enemy.Hero.mineCount - state.Hero.mineCount) * 20;
            var lifeDiff = enemy.Hero.life - state.Hero.life;
            var firstAttck = enemy.Path.Count == 1;
            var wishToAttack = 100 + (minesDiff - lifeDiff) + (firstAttck ? 30 : -30);
            if (state.Hero.life < 30)
                wishToAttack = 10;
            if (IsNearSpwanSpot(enemy.Hero, state))
                wishToAttack -= 30;
            if (IsNearSpwanSpot(state.Hero, state))
                wishToAttack += 30;

            var tavernPos = state.NearestTavern.Last();
            bool enemyNearTavern = IsNearTavern(enemy.Hero, tavernPos);
            if (enemyNearTavern)
                wishToAttack -= 60;

            var rnd = new Random();
            var toDo = rnd.Next(100);

            if (enemy.Path.Count > 0)
            {
                if (toDo <= wishToAttack)
                    retVal = new ConflictResolveResult
                    {
                        Result = ActOnPos.ForceGo,
                        Pos = enemy.Path[0]
                    };
                else if (wishToAttack < 20)
                    retVal = new ConflictResolveResult
                    {
                        Result = ActOnPos.RiskyToGo,
                        Pos = enemy.Path[0]
                    };
            }
            if (enemy.Path.Count == 0 && !enemyNearTavern)
                retVal = new ConflictResolveResult
                {
                    Result = ActOnPos.ForceGo,
                    Pos = enemy.Hero.pos
                };

            Console.Out.WriteLine($"CONFLICT enemy life:{enemy.Hero.life} enemy mines:{enemy.Hero.mineCount} Action:{retVal.Result} Pos:{retVal.Pos}");

            return retVal;
        }

        private bool IsNearSpwanSpot(Hero hero, CurrentState state)
        {
            var distance = ((hero.pos.x - hero.spawnPos.x)*(hero.pos.x - hero.spawnPos.x)) +
                           ((hero.pos.y - hero.spawnPos.y)*(hero.pos.y - hero.spawnPos.y));
            var maxDistance = state.MapSize*state.MapSize;
            return ((double) distance/(double) maxDistance)*100 < 10;
        }

        private bool IsNearTavern(Hero hero, Pos tavernPos)
        {
            return (Math.Abs(tavernPos.x-hero.pos.x) + Math.Abs(tavernPos.y - hero.pos.y) == 1);
        }
    }
}
