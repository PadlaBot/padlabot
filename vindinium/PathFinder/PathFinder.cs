﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using vindinium.PathFinder;

namespace vindinium.PathFinder
{
    public class PathFinder : IPathFinder
    {
        public Tile[][] Board { get; }

        public PathFinder(Tile[][] board)
        {
            this.Board = board;
        }
        public List<Pos> GetNearest(Pos from, Tile objectType, bool skipFirst = false)
        {
            var objects = new List<Pos>();
            for (int x = 0; x < Board.Length; x++)
                for (int y = 0; y < Board.Length; y++)
                    if (Board[x][y] == objectType)
                        objects.Add(new Pos { x = x, y = y, });
            if (!objects.Any())
                return null;
#if DEBUG
            var paths = objects.Select(pos => GetPathForNearest(from, pos)).ToList();
            var ordered = paths.OrderBy(path => path == null ? 999 : path.Count()).ToList();
            if (skipFirst)
                ordered = ordered.Skip(1).ToList();
            var first = ordered.First();
            return first?.ToList();
#else
            return objects.Select(pos => GetPathForNearest(from, pos))
                         .OrderBy(path => path == null ? 999 : path.Count())
                         .First()?.ToList();
#endif

            /*
            return objects.Select(pos => new { to = pos, paths = GetPathForNearest(from, pos)})
                                     .OrderBy(tile => tile.paths == null ? 999 : tile.paths.Count())
                                     .First().paths.ToList();*/
        }

        public List<Pos> GetPath(Pos from, Pos to)
        {
            return GetPathWithAction(from, to, (grid, tile, tileId, tileLineId) =>
            {
                switch (tile)
                {
                    case Tile.FREE:
                        grid[tileLineId, tileId] = new PathNode()
                        {
                            IsWall = false,
                            X = tileLineId,
                            Y = tileId,
                        };
                        break;
                    case Tile.HERO_1:
                    case Tile.HERO_2:
                    case Tile.HERO_3:
                    case Tile.HERO_4:
                    case Tile.GOLD_MINE_1:
                    case Tile.GOLD_MINE_2:
                    case Tile.GOLD_MINE_3:
                    case Tile.GOLD_MINE_4:
                    case Tile.GOLD_MINE_NEUTRAL:
                    case Tile.IMPASSABLE_WOOD:
                    case Tile.TAVERN:
                        grid[tileLineId, tileId] = new PathNode()
                        {
                            IsWall = true,
                            X = tileLineId,
                            Y = tileId,
                        };
                        break;
                    default:
                        throw new Exception("Unknown tile ?");
                }
                grid[to.x, to.y] = new PathNode()
                {
                    IsWall = false,
                    X = to.x,
                    Y = to.y,
                };
            });
        }

        private IEnumerable<Pos> GetPathForNearest(Pos from, Pos to)
        {
            return GetPathWithAction(from, to, (grid, tile, tileId, tileLineId) =>
            {
                switch (tile)
                {
                    case Tile.FREE:
                        grid[tileLineId, tileId] = new PathNode()
                        {
                            IsWall = false,
                            X = tileLineId,
                            Y = tileId,
                        };
                        break;
                    case Tile.HERO_1:
                    case Tile.HERO_2:
                    case Tile.HERO_3:
                    case Tile.HERO_4:
                    case Tile.GOLD_MINE_1:
                    case Tile.GOLD_MINE_2:
                    case Tile.GOLD_MINE_3:
                    case Tile.GOLD_MINE_4:
                    case Tile.GOLD_MINE_NEUTRAL:
                    case Tile.TAVERN:
                   
                    case Tile.IMPASSABLE_WOOD:
                        grid[tileLineId, tileId] = new PathNode()
                        {
                            IsWall = true,
                            X = tileLineId,
                            Y = tileId,
                        };
                        break;
                    default:
                        throw new Exception("Unknown tile ?");
                }
                grid[to.x,to.y] = new PathNode()
                {
                    IsWall = false,
                    X = to.x,
                    Y = to.y,
                };
            });
        }
        private List<Pos> GetPathWithAction(Pos from, Pos to, Action<PathNode[,], Tile, int, int> actionOnGrid)
        {
            var fromReversed = new Point(from.x, from.y);
            var toReversed = new Point(to.x, to.y);

            if (Board == null && Board.Length <= 0)
                return null;

            int width = Board.Length;
            int height = Board[0].Length;

            var grid = new PathNode[width, height];

            for (int tileLineId = 0; tileLineId < width; tileLineId++)
            {
                Tile[] tileLine = Board[tileLineId];
                for (int tileId = 0; tileId < tileLine.Length; tileId++)
                {
                    Tile tile = tileLine[tileId];
                    actionOnGrid(grid, tile, tileId, tileLineId);
                }
            }

           
            var finder = new Solver<PathNode, Object>(grid);

            var path = finder.Search(inStartNode: fromReversed,
                                 inEndNode: toReversed,
                                 inUserContext: null);

            return path == null ? null : path.Skip(1).Select(node => new Pos() { x = node.X, y = node.Y }).ToList();
        }

        public List<Pos> GetPathForRealMap(Pos from, Pos to)
        {
            var paths = GetPath(from: new Pos { x = from.y, y = from.x },
                           to: new Pos { x = to.y, y = to.x });
            if (paths != null && paths.Any())
            {
                var reversed = paths.Skip(1).Select(p => new Pos { x = p.y, y = p.x }).ToList();
                return reversed;
            }

            return null;
        }

        public List<Pos> GetNearestForRealMap(Pos from, Tile objectType, bool skipFirst = false)
        {
            var paths = GetNearest(from: new Pos { x = from.y, y = from.x },
                           objectType: objectType, skipFirst: skipFirst);

            if (paths != null && paths.Any())
            {
                var reversed = paths.Select(p => new Pos { x = p.y, y = p.x }).ToList();
                return reversed;
            }
            return null;
        }
    }
}
