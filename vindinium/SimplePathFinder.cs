﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vindinium
{
    class SimplePathFinder : IPathFinder
    {
        private Tile[][] _board;
        public SimplePathFinder(Tile[][] board)
        {
            _board = board;
        }

        public List<Pos> GetNearest(Pos from, Tile objectType)
        {
            var boardSize = _board.Length;
            int maxX = Math.Max(boardSize - from.x, from.x);
            int maxY = Math.Max(boardSize - from.y, from.y);
            for(int i=0; i<maxX; i++)
                for (int y = 0; y < maxY; y++)
                {
                    //if ()
                }
            return null;
        }

        public List<Pos> GetNearest(Pos from, Tile objectType, bool skipFirst = false)
        {
            throw new NotImplementedException();
        }

        public List<Pos> GetNearestForRealMap(Pos from, Tile objectType)
        {
            return null;
        }

        public List<Pos> GetNearestForRealMap(Pos from, Tile objectType, bool skipFirst = false)
        {
            throw new NotImplementedException();
        }

        public List<Pos> GetPath(Pos from, Pos to)
        {
            return null;
        }

        public List<Pos> GetPathForRealMap(Pos from, Pos to)
        {
            return null;

        }
    }
}


