﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace vindinium
{
    class Strategy
    {
        private Random _random;
        private CurrentState _lastState;
        private PriorityOrder.PriorityTarget _lastTarget;
        private PriorityOrder _priorityOrder;
        public List<PriorityOrder.PriorityTarget> Priorities { get; private set; }
        public PriorityOrder.PriorityTarget SelectedPriority { get; private set; }
        public List<Pos> Path { get; private set; }
        private readonly ConflictResolver conflictResolver = new ConflictResolver();
        private int stepCount;
        public Strategy(PriorityOrder priorityOrder)
        {
            _random = new Random();
            _priorityOrder = priorityOrder;
        }

        public string GetNextStep(CurrentState state)
        {
            stepCount++;
            if (stepCount > 1)
            {
                
            }
            ConflictResolver.ConflictResolveResult conflictResuilt = null;
            Pos avoid = null;
            var conflictWith = conflictResolver.IsConflict(state);
            if (conflictWith != null)
            {
                conflictResuilt = conflictResolver.ActOnConflict(conflictWith, state);
                switch (conflictResuilt.Result)
                {
                    case ConflictResolver.ActOnPos.ForceGo:
                        if (state.Hero.life > 35)
                            return getNextDirection(state.Hero.pos, conflictResuilt.Pos);
                        return getNextDirection(state.Hero.pos, state.NextNearestTavern);
                        
                    //case ConflictResolver.ActOnPos.Stay: return Direction.Stay;
                    case ConflictResolver.ActOnPos.RiskyToGo: avoid = conflictResuilt.Pos;break;
                }
            }

            Priorities = _priorityOrder.GetPriorities(state, _lastTarget);
            foreach (var target in Priorities)
            {
                var priorityTarget = target;
                if (conflictResuilt != null && conflictResuilt.Result == ConflictResolver.ActOnPos.RiskyToGo)
                {
                    priorityTarget = PriorityOrder.PriorityTarget.Life;
                    avoid = null;
                }

                string step = string.Empty;
                switch (priorityTarget)
                {
                    case PriorityOrder.PriorityTarget.Gold:
                        if (checkHealth(state.Hero, state.NearestGold))
                            step = getNextDirection(state.Hero.pos, state.NearestGold, avoid);
                        else
                        {
                            step = getNextDirection(state.Hero.pos, state.NearestTavern, avoid);
                            if (string.IsNullOrEmpty(step))
                                step = getNextDirection(state.Hero.pos, state.NextNearestTavern);
                        }
                        break;
                    case PriorityOrder.PriorityTarget.Life:
                        step = getNextDirection(state.Hero.pos, state.NearestTavern, avoid);
                        if (string.IsNullOrEmpty(step))
                            step = getNextDirection(state.Hero.pos, state.NextNearestTavern);
                        break;
                    case PriorityOrder.PriorityTarget.Hero1: step = getNextDirection(state.Hero.pos, state.Enemies.FirstOrDefault(x => x.Hero.id == 1)?.Path); break;
                    case PriorityOrder.PriorityTarget.Hero2: step = getNextDirection(state.Hero.pos, state.Enemies.FirstOrDefault(x => x.Hero.id == 2)?.Path); break;
                    case PriorityOrder.PriorityTarget.Hero3: step = getNextDirection(state.Hero.pos, state.Enemies.FirstOrDefault(x => x.Hero.id == 3)?.Path); break;
                    case PriorityOrder.PriorityTarget.Hero4: step = getNextDirection(state.Hero.pos, state.Enemies.FirstOrDefault(x => x.Hero.id == 4)?.Path); break;
                }
                if (!string.IsNullOrEmpty(step))
                {
                    SelectedPriority = priorityTarget;
                    return step;
                }
            }

            _lastState = state;
            _lastTarget = Priorities.First();
            return Direction.Stay;
        }

        internal string getNextDirection(Pos pc, List<Pos> path, Pos avoid = null)
        {
            Path = path;
            if (path == null || path.Count == 0)
                return string.Empty;
            var pn = path[0];
            if (avoid != null && avoid.IsEqual(pn) && path.Count > 1)
                return string.Empty;
            return getNextDirection(pc, pn);
        }

        internal bool checkHealth(Hero me, List<Pos> path)
        {
            return path == null ? true : (me.life > path.Count + 20 ? true : false);
        }

        internal string getNextDirection(Pos pc, Pos pn)
        {
            return (pc.x == pn.x ? (pc.y > pn.y ? Direction.West : Direction.East)
                : (pc.x > pn.x ? Direction.North : Direction.South));

        }
    }
}
