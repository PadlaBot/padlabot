﻿using System.Collections.Generic;
using System.Linq;

namespace vindinium
{
    class CurrentState
    {
        private GameResponse _response;
        private IPathFinder _finder;
        private HashSet<Tile> _minesToOccupy;
        public CurrentState(GameResponse response, IPathFinder finder)
        {
            _response = response;
            _finder = finder;
            _minesToOccupy = new HashSet<Tile>
            {
                Tile.GOLD_MINE_NEUTRAL
            };
            _minesToOccupy.Add(_response.hero.id == 1 ? Tile.GOLD_MINE_NEUTRAL : Tile.GOLD_MINE_1);
            _minesToOccupy.Add(_response.hero.id == 2 ? Tile.GOLD_MINE_NEUTRAL : Tile.GOLD_MINE_2);
            _minesToOccupy.Add(_response.hero.id == 3 ? Tile.GOLD_MINE_NEUTRAL : Tile.GOLD_MINE_3);
            _minesToOccupy.Add(_response.hero.id == 4 ? Tile.GOLD_MINE_NEUTRAL : Tile.GOLD_MINE_4);
        }

        private Tile HeroIdToMineTile(int id)
        {
            switch (id)
            {
                case 1: return Tile.GOLD_MINE_1;
                case 2: return Tile.GOLD_MINE_2;
                case 3: return Tile.GOLD_MINE_3;
                case 4: return Tile.GOLD_MINE_4;
            }
            return Tile.FREE;
        }

        public Hero Hero => _response.hero;
        public int MapSize => _response.game.board.size;

        public Board Board => _response.game.board;

        public List<Pos> NearestTavern => _finder.GetNearestForRealMap(_response.hero.pos, Tile.TAVERN) ?? new List<Pos>();
        public List<Pos> NextNearestTavern => _finder.GetNearestForRealMap(_response.hero.pos, Tile.TAVERN, true) ?? new List<Pos>();

        public List<Pos> NearestGold
        {
            get
            {
                var exclude = Enemies.OrderBy(x => x.Hero.mineCount).First().Hero.id;
                var excludeHero = HeroIdToMineTile(exclude);
                Tile mineType = Tile.GOLD_MINE_NEUTRAL;
                List<Pos> retVal = null;
                foreach (var mine in _minesToOccupy)
                {
                    if (mine == excludeHero)
                        continue;
                    var path = _finder.GetNearestForRealMap(_response.hero.pos, mine);
                    if (path == null)
                        continue;
                    if (retVal == null)
                    {
                        retVal = path;
                        mineType = mine;
                    }
                    else
                    {
                        if (path.Count > retVal.Count)
                            continue;
                        if (path.Count < retVal.Count || mineType == Tile.GOLD_MINE_NEUTRAL)
                        {
                            retVal = path;
                            mineType = mine;
                        }
                    }
                }
                return retVal ?? new List<Pos>();
            }
        }

        public List<EnemyInfo> Enemies
        {
            get
            {
                List<EnemyInfo> retVal = new List<EnemyInfo>();
                foreach (var hero in _response.game.heroes)
                {
                    if (hero.id == _response.hero.id)
                        continue;
                    retVal.Add(new EnemyInfo(hero, _finder.GetPathForRealMap(_response.hero.pos, hero.pos)));
                }
                return retVal;
            }
        }

    }
}
