﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vindinium
{
    public static class Extensions
    {
        public static void printDebugMap(IEnumerable<Pos> path, Tile[][] board)
        {

            byte[,] debugGrid = new byte[board.Length, board[0].Length];

            if (path != null)
            {
                Debug.WriteLine("Printing path size: " + path.Count());

                foreach (var node in path)
                {
                    Debug.WriteLine(node.x + " " + node.y + ", ");
                    debugGrid[node.x, node.y] = 1;
                }
                Debug.WriteLine("");
            }
            else Debug.WriteLine("path is null");

            for (int tileLineId = 0; tileLineId < board.Length; tileLineId++)
            {
                Tile[] tileLine = board[tileLineId];

                var builder = new StringBuilder();
                for (int tileId = 0; tileId < tileLine.Length; tileId++)
                {
                    Tile tile = tileLine[tileId];
                    builder.Append(printUsualTile(tile,
                            isThisPath: debugGrid[tileLineId, tileId] == 1));
                }

                Debug.WriteLine(builder.ToString());
            }
        }

        private static string printUsualTile(Tile tile, bool isThisPath)
        {
            switch (tile)
            {
                case Tile.FREE:
                    return isThisPath ? " *" : "- ";
                case Tile.HERO_1:
                    return isThisPath ? "1*" : "1 ";
                case Tile.HERO_2:
                    return isThisPath ? "2*" : "2 ";
                case Tile.HERO_3:
                    return isThisPath ? "3*" : "3 ";
                case Tile.HERO_4:
                    return isThisPath ? "4*" : "4 ";
                case Tile.GOLD_MINE_1:
                    return "G1";
                case Tile.GOLD_MINE_2:
                    return "G2";
                case Tile.GOLD_MINE_3:
                    return "G3";
                case Tile.GOLD_MINE_4:
                    return "G4";
                case Tile.GOLD_MINE_NEUTRAL:
                    return "GN";
                case Tile.IMPASSABLE_WOOD:
                    return "X ";
                case Tile.TAVERN:
                    return "T ";
                default:
                    throw new Exception("Unknown tile ?");
            }
        }
        public static string ConvertToStringMap(this string mapInOnRow, int spliceLength)
        {
            var mapRows = Extensions.Splice(mapInOnRow, spliceLength);
            return mapRows.Aggregate((current, next) => current + "\n" + next);
        }

        private static IEnumerable<string> Splice(this string s, int spliceLength)
        {
            if (s == null)
                throw new ArgumentNullException("s");
            if (spliceLength < 1)
                throw new ArgumentOutOfRangeException("spliceLength");

            if (s.Length == 0)
                yield break;
            var start = 0;
            for (var end = spliceLength; end < s.Length; end += spliceLength)
            {
                yield return s.Substring(start, spliceLength);
                start = end;
            }
            yield return s.Substring(start);
        }


        public static Tile[][] CreateBoard(int size, string data)
        {
            var output = data.Splice(size * 2);
            foreach (var item in output)
            {
                Console.WriteLine(item);
                Debug.WriteLine(item);
            }

            var board = new Tile[size][];
            //check to see if the board list is already created, if it is, we just overwrite its values
            board = new Tile[size][];

            //need to initialize the lists within the list
            for (int i = 0; i < size; i++)
            {
                board[i] = new Tile[size];
            }

            //convert the string to the List<List<Tile>>
            int x = 0;
            int y = 0;
            char[] charData = data.ToCharArray();

            for (int i = 0; i < charData.Length; i += 2)
            {
                switch (charData[i])
                {
                    case '#':
                        board[x][y] = Tile.IMPASSABLE_WOOD;
                        break;
                    case ' ':
                        board[x][y] = Tile.FREE;
                        break;
                    case '@':
                        switch (charData[i + 1])
                        {
                            case '1':
                                board[x][y] = Tile.HERO_1;
                                break;
                            case '2':
                                board[x][y] = Tile.HERO_2;
                                break;
                            case '3':
                                board[x][y] = Tile.HERO_3;
                                break;
                            case '4':
                                board[x][y] = Tile.HERO_4;
                                break;

                        }
                        break;
                    case '[':
                        board[x][y] = Tile.TAVERN;
                        break;
                    case '$':
                        switch (charData[i + 1])
                        {
                            case '-':
                                board[x][y] = Tile.GOLD_MINE_NEUTRAL;
                                break;
                            case '1':
                                board[x][y] = Tile.GOLD_MINE_1;
                                break;
                            case '2':
                                board[x][y] = Tile.GOLD_MINE_2;
                                break;
                            case '3':
                                board[x][y] = Tile.GOLD_MINE_3;
                                break;
                            case '4':
                                board[x][y] = Tile.GOLD_MINE_4;
                                break;
                        }
                        break;
                }

                //time to increment x and y
                x++;
                if (x == size)
                {
                    x = 0;
                    y++;
                }
            }

            return board;
        }
    }
}
