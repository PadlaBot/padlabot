﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using vindinium.PathFinder;

namespace vindinium
{
    class AgressiveMinerBot
    {
        private ServerStuff _serverStuff;
        public AgressiveMinerBot(ServerStuff serverStuff)
        {
            _serverStuff = serverStuff;
        }

        public void Run()
        {
            Console.Out.WriteLine("Agressive bot start");
            _serverStuff.createGame();
            if (_serverStuff.errored == false)
            {
                //opens up a webpage so you can view the game, doing it async so we dont time out
                new Thread(delegate ()
                {
                    System.Diagnostics.Process.Start(_serverStuff.viewURL);
                }).Start();
            }
            var pathFinder = new PathFinder.PathFinder(_serverStuff.board);
            var priorityManager = new PriorityOrder(120, 60, 40, 10);
            var strategy = new Strategy(priorityManager);

            while (_serverStuff.finished == false && _serverStuff.errored == false)
            {
                var state = new CurrentState(_serverStuff.GameResponse, pathFinder);
                var step = strategy.GetNextStep(state);
                var pathStr = strategy.Path == null ? "-" : string.Join(",", strategy.Path);
                string priorityStr = strategy.Priorities == null || !strategy.Priorities.Any() ? "-" : strategy.Priorities.First().ToString();

                Console.Out.WriteLine($"Life:{state.Hero.life} Mines:{state.Hero.mineCount} Target:{priorityStr} SelectedTarget:{strategy.SelectedPriority} Step:{step} Pos:{state.Hero.pos}");
                _serverStuff.moveHero(step);
            }
            if (_serverStuff.errored)
                Console.Out.WriteLine($"ERROR: {_serverStuff.errorText}");
        
        }
    }
}
