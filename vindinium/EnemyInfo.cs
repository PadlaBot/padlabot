﻿using System.Collections.Generic;

namespace vindinium
{
    class EnemyInfo
    {
        public Hero Hero { get; private set; }
        public List<Pos> Path { get; private set; }

        public EnemyInfo(Hero hero, List<Pos> path)
        {
            Hero = hero;
            Path = path;
        }
    }
}
