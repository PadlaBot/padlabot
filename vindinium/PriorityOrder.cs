﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vindinium
{
    class PriorityOrder
    {
        public enum PriorityTarget
        {
            Gold,
            Life,
            Hero1,
            Hero2,
            Hero3,
            Hero4
        }

        private int _goldDefaultPriority;
        private int _healthDefaultPriority;
        private int _enemyDefaultPriority;
        private int _priorityStep;
        public PriorityOrder(int goldDefaultPriority, int healthDefaultPriority, int enemyDefaultPriority, int priorityStep)
        {
            _goldDefaultPriority = goldDefaultPriority;
            _healthDefaultPriority = healthDefaultPriority;
            _enemyDefaultPriority = enemyDefaultPriority;
            _priorityStep = priorityStep;
        }

        public List<PriorityTarget> GetPriorities(CurrentState state, PriorityTarget lastTrg)
        {
            var targetPriorities = new Dictionary<PriorityTarget, int>
            {
                {PriorityTarget.Gold, _goldDefaultPriority },
                {PriorityTarget.Life, _healthDefaultPriority },
                {PriorityTarget.Hero1, _enemyDefaultPriority },
                {PriorityTarget.Hero2, _enemyDefaultPriority },
                {PriorityTarget.Hero3, _enemyDefaultPriority },
                {PriorityTarget.Hero4, _enemyDefaultPriority },
            };

            // rise last target priority
            targetPriorities[lastTrg] += _priorityStep * 2;

            var enemiesTotalMines = state.Enemies.Sum(x => x.Hero.mineCount);
            targetPriorities[PriorityTarget.Gold] += (enemiesTotalMines - state.Hero.mineCount)*_priorityStep;
            if (state.NearestGold.Count < 4)
                targetPriorities[PriorityTarget.Gold] += 50;

            if (state.Hero.life < 21)
                targetPriorities[PriorityTarget.Gold] = 0;

            var heroToLive = state.Hero.life - state.NearestTavern.Count;
            if (heroToLive < 60)
            {
                targetPriorities[PriorityTarget.Life] += 60 - heroToLive;
            }
            else
            {
                targetPriorities[PriorityTarget.Life] = 0;
            }
            if (state.Hero.life < 70 && state.NearestTavern.Count < 2)
                targetPriorities[PriorityTarget.Life] += 300;

            foreach (var enemy in state.Enemies)
            {
                var key = HeroIdToEnum(enemy.Hero.id);
                targetPriorities[key] = CalcEnemyPriority(enemy.Hero, state.Hero, enemy.Path);
            }
            return targetPriorities.OrderByDescending(x => x.Value).Select(x => x.Key).ToList();
        }

        private PriorityTarget HeroIdToEnum(int id)
        {
            switch (id)
            {
                case 1: return PriorityTarget.Hero1;
                case 2: return PriorityTarget.Hero2;
                case 3: return PriorityTarget.Hero3;
                case 4: return PriorityTarget.Hero4;
            }
            throw new IndexOutOfRangeException("Max 4 heroes allowed");
        }

        private int CalcEnemyPriority(Hero enemy, Hero me, List<Pos> path)
        {
            int distance;
            if (path == null) distance = 100;
            else distance = path.Count;
            var retVal = _enemyDefaultPriority + ((enemy.mineCount - me.mineCount) * _priorityStep) - distance;
            retVal -= enemy.life - me.life;
            return retVal;
        }
    }
}
