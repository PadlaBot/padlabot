﻿using System.Collections.Generic;

namespace vindinium
{
    interface IPathFinder
    {
        List<Pos> GetPath(Pos from, Pos to);
        List<Pos> GetPathForRealMap(Pos from, Pos to);
        List<Pos> GetNearest(Pos from, Tile objectType, bool skipFirst = false);
        List<Pos> GetNearestForRealMap(Pos from, Tile objectType, bool skipFirst = false);
    }
}
